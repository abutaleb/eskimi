
# Campaign Tracker


This is a Laravel application that tracks campaign data and aggregates it to a table. It uses a third-party API to fetch country data from IP addresses and caches the results for faster processing. The app also uses Redis for fast storage.



## Prerequisites


- PHP 8.1 or higher
- Redis
- MySQL
- Composer ^2
## Tech Stack

**Front-end:** JavaScript

**Backend:** PHP 8.1, Laravel

**Database:** MySQL, Redis


## Installation

### JavaScript Tracker ### 
```bash
JavaScript tracker code located in https://gitlab.com/abutaleb/eskimi/-/blob/main/TrackerJS/index.html
```
### Laravel Backend ### 

**1. Clone the project**

```bash
  git clone https://gitlab.com/abutaleb/eskimi.git
```

**2. Go to the project directory**

```bash
  cd eskimi
```

**3. Install dependencies**

```bash
 composer install

```

**4. Copy the .env.example file and create a new .env file:**

```bash
 cp .env.example .env
```

**5. Generate a new application key:**

```bash
php artisan key:generate
```
**6. Update the .env file with your database and Redis configuration:**

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database_name
DB_USERNAME=your_database_username
DB_PASSWORD=your_database_password

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

```
**7. Migrate the database:**

```bash
php artisan migrate
```

**8. Run the Laravel development server:**

```bash
php artisan serve --port=3000
```

**9. Start the Laravel queue worker with new terminal:**

```bash
php artisan queue:work
```

**10.** Access the app in your web browser at http://localhost:3000
## Testing

To run tests, run the following command

```bash
 php artisan test
```


## License

[MIT](https://choosealicense.com/licenses/mit/)


## API Reference

#### Track Click Info

```http
  GET http://localhost:3000/track?cid=4235&crid=23423&bid=5&did=8&cip=78.60.201.201&conv=imp
```


## Authors

- [@abutaleb](https://gitlab.com/mdabutaleb)
