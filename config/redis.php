<?php

return [
    'client' => env('REDIS_CLIENT', 'predis'),

    'connection_type' => ['default', 'cache', 'queue'],

    'default' => [
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'port' => env('REDIS_PORT', 6379),
        'password' => env('REDIS_PASSWORD', null),
        'prefix' => env('REDIS_PREFIX', ''),
        'database' => env('REDIS_DATABASE', null),
        // Sentinel config
        'use_sentinel' => env('REDIS_IS_SENTINEL', 0),
        'service_name' => env('REDIS_SENTINEL_SERVICE_NAME', 'redis-cluster'),
        // RedisLab config
        'use_redislab' => env('REDIS_IS_REDISLAB', 0),
        'ca_file' => env('REDIS_CAFILE_LOCATION', ''),
        'verify_peer' => env('REDIS_VERIFY_PEER', ''),
        'private_key_location' => env('REDIS_PRIVATE_KEY_LOCATION', ''),
        'cert_file_location' => env('REDIS_CERT_FILE_LOCATION', ''),
    ],

    'cache' => [
        'host' => env('REDIS_CACHE_HOST', '127.0.0.1'),
        'port' => env('REDIS_CACHE_PORT', 6379),
        'password' => env('REDIS_CACHE_PASSWORD', null),
        'prefix' => env('REDIS_CACHE_PREFIX', ''),
        'database' => env('REDIS_CACHE_DATABASE', null),
        // Sentinel config
        'use_sentinel' => env('REDIS_CACHE_IS_SENTINEL', 0),
        'service_name' => env('REDIS_CACHE_SENTINEL_SERVICE_NAME', 'redis-cluster'),
        // RedisLab config
        'use_redislab' => env('REDIS_CACHE_IS_REDISLAB', 0),
        'ca_file' => env('REDIS_CACHE_CAFILE_LOCATION', ''),
        'verify_peer' => env('REDIS_CACHE_VERIFY_PEER', ''),
        'private_key_location' => env('REDIS_CACHE_PRIVATE_KEY_LOCATION', ''),
        'cert_file_location' => env('REDIS_CACHE_CERT_FILE_LOCATION', ''),
    ],

];
