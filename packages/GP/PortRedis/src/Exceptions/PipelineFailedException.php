<?php

namespace GP\PortRedis\Exceptions;

use Exception;

class PipelineFailedException extends Exception
{
}
