<?php

declare(strict_types=1);

namespace GP\PortRedis;

use Predis;
use Exception;
use Throwable;

class PortRedis
{
    /**
     *
     * @var PortRedisClient
     */
    protected $client;
    private $prefix;
    private $connectionType;
    private $error = 0;

    public function __construct($connectionType = null)
    {
        if ($connectionType != null) {
            $this->fromConfig($connectionType);
        }
    }

    /*
     * @throws Exception
     */
    public function fromConfig(string $key, array $overridable = [])
    {
        $this->setConnectionType($key);

        $config = config('redis.' . $key);

        $this->setPrefix($config);
        $connection = $this->getRedisConnections($config);
        $options = $this->getRedisOptions($config, $overridable);

        return $this->loadConfiguration($connection, $options);
    }

    public function injectConfig($connection, $options)
    {
        return $this->loadConfiguration($connection, $options);
    }

    private function loadConfiguration($config, $options)
    {
        try {
            $this->client = new PortRedisClient($config, $options);
        } catch (Exception $exception) {
            $this->writeError($exception, 'Connection Error', __METHOD__);
            return false;
        }
    }

    public function init($key)
    {
        if ($this->client == null) {
            $this->fromConfig('default');
        }

        return $this->prefix . $key;
    }
    /*
     * @throws Exception
     */

    public function setConnectionType($connectionType)
    {
        $connectionTypeList = config('redis.connection_type');

        if (!in_array($connectionType, $connectionTypeList)) {
            throw new Exception('Provided Redis connection is not supported by PortRedis');
        }

        $this->connectionType = $connectionType;
    }

    public function setPrefix(array $config)
    {
        $this->prefix = $config['prefix'] ?? '';
    }

    //    public function set(string $key, $value)
    //    {
    //        try {
    //            $key = $this->init($key);
    //
    //            return $this->client->set($key, json_encode($value));
    //        } catch (Exception $exception) {
    //            $this->writeError($exception, $key, __METHOD__);
    //
    //            return false;
    //        }
    //    }

    /**
     * Fetches from Redis
     *
     * @param $key
     * @return bool|mixed
     */
    public function get($key)
    {
        $fetchedData = false;
        $key = $this->init($key);

        try {
            $result = $this->client->get($key);

            if (empty($result)) {
                return $fetchedData;
            }

            $fetchedData = json_decode($result, true);

            if (empty($fetchedData)) {
                return $result;
            }
        } catch (Predis\Connection\ConnectionException | Throwable | Exception $exception) {
            $this->writeError($exception, $key, __METHOD__);
        }

        return $fetchedData;
    }

    /**
     * Update/set data
     *
     * @param $key
     * @param $value
     * @param int $expiry
     * @param bool $expireAt
     * @param bool $doNoExpire
     * @return bool
     */
    public function put($key, $value, $expiry = 3600, $expireAt = false, $doNoExpire = false)
    {
        $key = $this->init($key);

        try {
            $ret = $this->client->set($key, json_encode($value));

            if (!$doNoExpire) {
                if ($expireAt) {
                    $this->client->expireAt($key, $expireAt);
                } else {
                    $expiry = $this->expireWithAI($key, $expiry);
                    $this->client->expire($key, $expiry);
                }
            }

            return $ret;
        } catch (Predis\Connection\ConnectionException | Exception $exception) {
            $this->writeError($exception, $key, __METHOD__);

            return false;
        }
    }

    /**
     * Deletes Key from Redis
     *
     * @param $key
     * @return bool
     */
    public function delete($key)
    {
        $key = $this->init($key);
        try {
            return $this->client->del($key);
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception, $key, __METHOD__);

            return false;
        } catch (\Exception $exception) {
            $this->writeError($exception, $key, __METHOD__);

            return false;
        }
    }

    /**
     * Increment Numeric data by 1
     *
     * @param $key
     * @param int $expiry
     * @param bool $expireAt
     * @param bool $doNoExpire
     * @return bool
     */
    public function incr($key, $expiry = 3600, $expireAt = false, $doNoExpire = false)
    {
        $key = $this->init($key);

        try {
            $response = $this->client->incr($key);
            if (!$doNoExpire) {
                if ($expireAt) {
                    $this->client->expireAt($key, $expireAt);
                } else {
                    $expiry = $this->expireWithAI($key, $expiry);
                    $this->client->expire($key, $expiry);
                }
            }

            return $response;
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception);

            return false;
        }
    }

    public function expireAt($key, $timestamp)
    {
        $key = $this->init($key);

        try {
            return $this->client->expireAt($key, $timestamp);
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception);

            return false;
        }
    }

    public function incrBy($key, $value, $expiry = 3600, $expireAt = false, $doNoExpire = false)
    {
        $key = $this->init($key);

        try {
            $response = $this->client->incrBy($key, $value);
            if (!$doNoExpire) {
                if ($expireAt) {
                    $this->client->expireAt($key, $expireAt);
                } else {
                    $expiry = $this->expireWithAI($key, $expiry);
                    $this->client->expire($key, $expiry);
                }
            }

            return $response;
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception);

            return false;
        }
    }

    /**
     * Subtracts Numeric data by 1
     *
     * @param $key
     * @param int $expiry
     * @param bool $expireAt
     * @param bool $doNoExpire
     * @return bool
     */
    public function decr($key, $expiry = 3600, $expireAt = false, $doNoExpire = false)
    {
        $key = $this->init($key);
        try {
            $ret = $this->client->decr($key);
            if (!$doNoExpire) {
                if ($expireAt) {
                    $this->client->expireAt($key, $expireAt);
                } else {
                    $expiry = $this->expireWithAI($key, $expiry);
                    $this->client->expire($key, $expiry);
                }
            }

            return $ret;
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception);

            return false;
        }
    }

    /**
     * Queue Push
     *
     * @param $key
     * @param $value
     * @return bool
     */
    public function lPush($key, $value)
    {
        $key = $this->init($key);

        try {
            return $this->client->lpush($key, json_encode($value));
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception, $key, __METHOD__);

            return false;
        } catch (\Exception $exception) {
            $this->writeError($exception, $key, __METHOD__);

            return false;
        }
    }

    /**
     * Queue Pop
     *
     * @param $key
     * @return bool|mixed
     */
    public function rPop($key)
    {
        $key = $this->init($key);
        try {
            return json_decode($this->client->rpop($key), true);
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception, $key, __METHOD__);

            return false;
        } catch (\Exception $exception) {
            $this->writeError($exception, $key, __METHOD__);

            return false;
        }
    }

    /**
     * Pops first item of Origin Queue and pushes to Destination Queue
     *
     * @param $origin
     * @param $destination
     * @return bool|false|string
     */
    public function rPopLpush($origin, $destination)
    {
        $origin = $this->prefix . $origin;
        $destination = $this->prefix . $destination;
        try {
            return @json_encode($this->client->rpoplpush($origin, $destination));
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception, $origin . '->' . $destination, __METHOD__);

            return false;
        } catch (\Exception $exception) {
            $this->writeError($exception, $origin . '->' . $destination, __METHOD__);

            return false;
        }
    }

    public function expireWithAI($key, $expiry)
    {
        $key = $this->prefix . $key;

        $targets = array('catalogs', 'cards');
        $shouldOverride = false;
        foreach ($targets as $target) {
            if (strstr($key, $target) !== false) {
                $shouldOverride = true;
                break;
            }
        }
        if (!$shouldOverride) {
            return $expiry;
        }
        $expireAt = time() + $expiry;
        $expireAtHour = date('H', $expireAt);
        if ($expireAtHour > 18 && $expireAtHour < 22) {
            $extendHour = 22 - $expireAtHour;
            $expiry += $extendHour * 3600;
        }
        if ($expireAtHour >= 12 && $expireAtHour < 14) {
            $extendHour = 14 - $expireAtHour;
            $expiry += $extendHour * 3600;
        }

        return $expiry;
    }

    public function ttl($key)
    {
        $key = $this->init($key);
        try {
            return $this->client->ttl($key);
        } catch (Predis\Connection\ConnectionException $exception) {
            $this->writeError($exception);

            return false;
        }
    }

    public function __call($name, $args)
    {
        try {
            return $this->client->{$name}(...$args);
        } catch (Exception $exception) {
            $this->writeError($exception, $name, __METHOD__);

            return false;
        }
    }

    protected function writeError($exception, $key = '', $method = '')
    {
        $this->error = 1;

        try {
            $logMessage = time() . ' ' . $method . '@' . $key . "@" . $exception->getMessage() . "\n";
            app('log')->error($logMessage);
        } catch (Exception $ex) {
            return false;
        }
    }

    public function getRedisConnections(array $config)
    {
        $connections = [];
        $isRedisLab = $config['use_redislab'] ?? 0;

        if ($isRedisLab == 1) {
            $connections['scheme'] = 'tls';
            $connections['host'] = $config['host'] ?? 'redis';
            $connections['port'] = $config['port'] ?? 10496;
            $connections['ssl'] = [
                'cafile' => $config['ca_file'],
                'verify_peer' => $config['verify_peer'] ?? false,
                'local_pk' => $config['private_key_location'] ?? '',
                'local_cert' => $config['cert_file_location'] ?? ''
            ];
        } else {
            $ips = explode(',', $config['host']);
            $port = $config['port'] ?? '6379';

            if (count($ips) == 1) {

                $isSentinel = $config['use_sentinel'] ?? 0;

                if ($isSentinel != 0) {
                    $connections[] = "tcp://" . $ips[0] . ":" . $port;
                } else {
                    $connections = "tcp://" . $ips[0] . ":" . $port;
                }
            } else if (count($ips) > 1) {
                foreach ($ips as $item) {
                    $connections[] = "tcp://" . $item . ":" . $port;
                }
            }
        }

        return $connections;
    }

    public function getRedisOptions(array $config, array $overridable = [])
    {
        $option = [
            'parameters' => [
                'password' => $config['password'] ?? null,
                'timeout' => $overridable['timeout'] ?? $config['timeout'] ?? 0.200,
                'read_write_timeout' => $overridable['read_write_timeout'] ?? $config['read_write_timeout'] ?? 0.200,
                'database' => $overridable['database'] ?? $config['database'] ?? null,
            ]
        ];

        $isSentinel = $config['use_sentinel'] ?? 0;

        if ($isSentinel != 0) {
            $option['replication'] = 'sentinel';
            $option['service'] = $config['service_name'] ?? 'redis-cluster';
        }

        return $option;
    }
}
