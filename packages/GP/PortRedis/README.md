# MyGP PortRedis Library


### Installation

- Create a folder `packages/GP` in the application root folder (`codes/`) and place the `PortRedis` library code into the `packages/GP` folder.
- Update project root `composer.json` file as follow:

    ```
    "autoload": {
        "psr-4": {
            "App\\": "app/",
            "GP\\PortRedis\\": "packages/GP/PortRedis/src/"
        }
    }
    ```

- Run `composer dump-autoload` (if using docker, then run: `docker-compose exec app composer dump-autoload`) and voila!
- Copy the `GP/PortRedis/config/redis.php` file into the project root config directory.
- Set all required env variables (check the `config/redis.php` for understanding) in the `docker/.envs/app.env` or `codes/.env` (if non-dockerized) file.
- For lumen, add the below line in the `bootstrap/app.php` file:
    ```
    $app->configure('redis');
    ```

### Usage:

- We need to instantiate the PortRedis class with available connections. Currently, available connections are: "cache", "queue".
- If any more connections needed, we just need to update the `config/redis.php` of root config folder. 
- All existing methods that are used in previous `Redis` and `NewRedis` are also available in this library.

- Sample usage example:

    ```
    use GP\PortRedis\PortRedis;

    $redisInstance = new PortRedis('cache');
    $redisInstance->set('foo', 'bar');
    $redisInstance->get('foo');
    $redisInstance->delete('foo');


    $queueName = 'testQueue';
    $queueValue = 'testValue';

    $redisQueueInstance = new PortRedis('queue');
    $redisQueueInstance->lPush($queueName, json_encode($queueValue));
    ```

- For using Sentinel:
    - Set `REDIS_IS_SENTINEL`, `REDIS_CACHE_IS_SENTINEL`, `REDIS_QUEUE_IS_SENTINEL` value as 1.
    - Set `REDIS_SENTINEL_SERVICE_NAME`, `REDIS_CACHE_SENTINEL_SERVICE_NAME`, `REDIS_QUEUE_SENTINEL_SERVICE_NAME` from the Sentinel service name.

- Available new methods:
    - `fromConfig($configKey)`
        > This method will collect the redis configuration from the `codes/config/redis.php` file according to the `$configKey`.

    - `setPrefix($prefix)`
        > It will collect the prefix from the configuration key.

    - `setConnectionType($type)`
        > Available connections are defined in the `codes/config/redis.php` file. Need to update the `connection_type` array if any new connection has been added later.
