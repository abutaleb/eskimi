<?php

namespace App\Domain\Track\Repositories;

use GP\PortRedis\PortRedis;
use Illuminate\Support\Facades\Http;

class CountryDetailsRepository
{
    private const cashKeyPrefix = 'country_details:';

    public function __Construct()
    {
        $this->redis = new PortRedis('cache');
    }

    /**
     * @param string $ip
     * @return string|null
     */
    public function getCountryCodeByIp(string $ip): ?string
    {
        try {
            $countryCode = $this->getCountryCodeFromCash($ip);
            if (!empty($countryCode)) {
                return $countryCode;
            }
            $countryDetails = $this->getCountryCodeFromPlatformAndCache($ip);
            if (!empty($countryDetails['country_code'])) {
                return $countryDetails['country_code'];
            }

            return null;
        } catch (\Throwable $exception) {
            //     TODO::Write the log
        }


    }

    /**
     * @param string $ip
     * @return string
     */
    public function getCountryCodeFromCash(string $ip): ?string
    {
        try {
            $cache_key = self::cashKeyPrefix . $ip;
            return $this->redis->hget($cache_key, 'country_code');
        } catch (\Throwable $exception) {
//          TODO::Write to log
        }

    }

    /**
     * @param string $ip
     * @return array
     */
    public function getCountryCodeFromPlatformAndCache(string $ip): array
    {
        try {
            $request_uri = config('connector.ipfind.base_url');
            $response = Http::get($request_uri, ['ip' => $ip]);
            $result = json_decode($response->body(), true);

            $cache_key = self::cashKeyPrefix . $ip;
            $this->redis->pipeline(function ($pipe) use ($cache_key, $result) {
                foreach ($result as $key => $item) {
                    $pipe->hset($cache_key, $key, $item);
                }
            });
            return $result;
        } catch (\Throwable $exception) {
            //     TODO::Write the log
        }

    }

}
