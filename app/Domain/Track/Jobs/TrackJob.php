<?php

namespace App\Domain\Track\Jobs;

use App\Domain\Track\Models\CampaignTracking;
use App\Domain\Track\Repositories\CountryDetailsRepository;
use App\Domain\Track\Service\CountryDetailsService;
use GP\PortRedis\PortRedis;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TrackJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var PortRedis
     */
    private $redis;

    /**
     * @var array
     */
    public $data;
    /**
     * @var CountryDetailsService
     */
    private $countryDetailsService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->countryDetailsService = new CountryDetailsService(new CountryDetailsRepository());

    }

    /**
     * @return void
     */
    public function handle(): void
    {
        try {
            $this->prepareTrackingData();
            $data = $this->data;
//            dd($data);
            $item = CampaignTracking::where('date', $data['date'])
                ->where('country_code', $data['country_code'])
                ->where('campaign_id', $data['campaign_id'])
                ->where('creative_id', $data['creative_id'])
                ->where('browser_id', $data['browser_id'])
                ->where('device_id', $data['device_id'])
                ->first();
//            dd($item, __LINE__);
            if (!is_null($item)) {
                $item->increment('count');
                $item->update();
            } else {
                CampaignTracking::create($data);
            }
        } catch (\Throwable $exception) {
//           TODO::Exception log writing
        }
    }

    /**
     * @return void
     */
    private function prepareTrackingData(): void
    {
        $this->data['country_code'] = $this->countryDetailsService->getCountryCodeByIP($this->data['client_ip']);
        if (!empty($this->data['client_ip'])) {
            unset($this->data['client_ip']);
        }
    }


}
