<?php

namespace App\Domain\Track\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignTracking extends Model
{
    use HasFactory;
    protected $table = 'campaign_trackings';
    protected $fillable = ['date', 'country_code', 'campaign_id', 'creative_id', 'browser_id', 'device_id', 'count'];
}
