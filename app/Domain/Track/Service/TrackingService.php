<?php

namespace App\Domain\Track\Service;

use App\Domain\Track\Jobs\TrackJob;

class TrackingService
{
    public $data;


    /**
     * @return void
     */
    public function handle(): void
    {
        dispatch(new TrackJob($this->transformData($this->data)));
    }



    /**
     * @param array $data
     * @return array
     */
    private function transformData(array $data): array
    {
        return [
            'date' => date('y-m-d'),
            'campaign_id' => $data['cid'] ?? '',
            'creative_id' => $data['crid'] ?? '',
            'browser_id' => $data['bid'] ?? '',
            'device_id' => $data['did'] ?? '',
            'client_ip' => $data['cip'] ?? '',
        ];
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }


}
