<?php

namespace App\Domain\Track\Service;

use App\Domain\Track\Repositories\CountryDetailsRepository;

class CountryDetailsService
{
    /**
     * @param CountryDetailsRepository $countryDetailsRepository
     */
    public function __Construct(private CountryDetailsRepository $countryDetailsRepository)
    {

    }

    /**
     * @param string $ip
     * @return string|null
     */
    public function getCountryCodeByIP(string $ip): ?string
    {
        try {
           return  $this->countryDetailsRepository->getCountryCodeByIp($ip);
        } catch (\Throwable $exception) {
            //TODO::Write Log
            return null;
        }
    }

}
