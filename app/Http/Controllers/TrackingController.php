<?php

namespace App\Http\Controllers;

use App\Domain\Track\Service\TrackingService;
use Illuminate\Http\Request;

class TrackingController extends Controller
{
    /**
     * @param TrackingService $trackingService
     */
    public function __Construct(private TrackingService $trackingService)
    {

    }

    /**
     * @param Request $request
     * @return void
     */
    public function track(Request $request)
    {
      return  $this->trackingService->setData($request->all())->handle();
    }
}
