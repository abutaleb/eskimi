<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TrackingRouteTest extends TestCase
{

    /** @test* */
    public function test_track_route()
    {
        $response = $this->get('/track?cid=4235&crid=23423&bid=5&did=8&cip=78.60.201.201&conv=imp');
        $response->assertStatus(200);
    }

}
