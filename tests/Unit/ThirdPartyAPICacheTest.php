<?php

namespace Tests\Unit;

use App\Domain\Track\Repositories\CountryDetailsRepository;
use Tests\TestCase;

class ThirdPartyAPICacheTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_3rd_party_api_data_cache()
    {
        $client_ip = '78.60.201.201';
        $countryCodeFromPlatform = (new CountryDetailsRepository())->getCountryCodeFromPlatformAndCache($client_ip);
        $countryCodeFromCash = (new CountryDetailsRepository())->getCountryCodeFromCash($client_ip);
        $this->assertEquals($countryCodeFromCash, $countryCodeFromPlatform['country_code']);
    }
}
