<?php

namespace Tests\Unit;

use App\Domain\Track\Jobs\TrackJob;
use Tests\TestCase;
class AggregationTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_aggregation()
    {
        $inputData = [
            'date' => date('y-m-d'),
            'campaign_id' => 4235,
            'creative_id' => 23423,
            'browser_id' => 5,
            'device_id' => 8,
            'client_ip' => '78.60.201.201'
        ];

        $expectedAggregation  = [
            'date' => date('y-m-d'),
            'campaign_id' => 4235,
            'creative_id' => 23423,
            'browser_id' => 5,
            'device_id' => 8,
            'country_code' => 'LT'
        ];

        $job = new TrackJob($inputData);
        $job->handle();
        $this->assertEquals($expectedAggregation, $job->data);
    }
}
